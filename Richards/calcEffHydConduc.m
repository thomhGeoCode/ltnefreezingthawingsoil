% File written by Thomas Heinze, Bochum, Germany
% Copyright (c) 2021 Thomas Heinze
% Please cite as: 
% Thomas Heinze (2021) A multi-phase heat transfer model for water infiltration into frozen soil

function EffHydConduc = calcEffHydConduc(EffSat, satHydConduc, vanGenM, VolFracF)

  EffHydConduc = sqrt( EffSat ) .* satHydConduc .* ...
                 ( 1.0 - ( 1.0 - EffSat.^(1.0./vanGenM) ).^vanGenM ).^2.0 ...
                 .* 10.^(-7 .* VolFracF);

end