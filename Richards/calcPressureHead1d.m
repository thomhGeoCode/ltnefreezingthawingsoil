% File written by Thomas Heinze, Bochum, Germany
% Copyright (c) 2021 Thomas Heinze
% Please cite as: 
% Thomas Heinze (2021) A multi-phase heat transfer model for water infiltration into frozen soil

function PressureHead = calcPressureHead1d(PressureHeadOld, EffHydConduc, ...
  MoistCapac, Source, dt, dz)

  aa = 0.0 .* PressureHeadOld;
  bb = 0.0 .* PressureHeadOld;
  cc = 0.0 .* PressureHeadOld;
  dd = 0.0 .* PressureHeadOld;

  aa(2:end-1) = ( EffHydConduc(3:end) + EffHydConduc(2:end-1) ) .* 0.5;
  bb(2:end-1) = PressureHeadOld(3:end) - PressureHeadOld(2:end-1);
  cc(2:end-1) = ( EffHydConduc(1:end-2) + EffHydConduc(2:end-1) ) .* 0.5;
  dd(2:end-1) = PressureHeadOld(1:end-2) - PressureHeadOld(2:end-1);
  
  PressureHead = PressureHeadOld + dt ./ MoistCapac .* ( ...
                 aa .* bb ./ (dz * dz) + ...
                 cc .* dd ./ (dz * dz) - ...
                 ( aa - cc ) ./ dz ) + dt ./ MoistCapac .* Source;
  
end