% File written by Thomas Heinze, Bochum, Germany
% Copyright (c) 2021 Thomas Heinze
% Please cite as: 
% Thomas Heinze (2021) A multi-phase heat transfer model for water infiltration into frozen soil

function [Tselec, VolFracFreezing] = freezingRadii(waterContRes, ...
  maxWaterContSat, vanGenAlpha, vanGenN, vanGenM)

deltaTheta = 0.001;
ListDeltaTheta = waterContRes+deltaTheta:deltaTheta:maxWaterContSat-deltaTheta;

EffSat = calcEffSaturation(ListDeltaTheta, waterContRes, maxWaterContSat);

ListH = - (EffSat.^(-1.0/vanGenM) - 1.0).^(1.0/vanGenN) ./ vanGenAlpha;

%% Get radii from h
sigma = 7.27e-2; % J/m^2
% density of water (kg/m^3)
densW = 1000.0;
% pore radii existing
R = - 2.0 * sigma / densW / 9.81 ./ ListH;
% numbers of pores with the respective radius
N = deltaTheta ./ pi ./ (R.^2);
% Check if porosity is good
Porosity = pi * sum(N .* (R.^2) );
mean(R);
%% Get frozen pores at given temperatures
sigmaLs = 2.9e-2; %J/m^2
A = -10^(-19.5); %J
Tm = 273.15; %K
Lf = 334e3; %J/kg

% Given temperatures of phase transition
T = 273.10:-0.05:(273.15-7);
% Thickness of water layer
d = (-A/(6.0*pi*densW*Lf) .* (Tm ./ (Tm - T))).^(1/3);
% Minimum Pore radii that will freeze at the given T
% All radii > Rfreeze will be frozen
Rfreeze = sigmaLs ./ densW ./ Lf .* (Tm ./ (Tm - T)) + d;

VolFracF = zeros(length(Rfreeze),1);
for i = 1:length(Rfreeze)
  for j=1:length(R)
    if R(j) > Rfreeze(i)
      VolFracF(i) = VolFracF(i) + pi * N(j) * (R(j)-d(i))^2;
    end
  end
end

%%
% figure()
% plot(T, VolFracF)
% hold on
% plot(T,maxWaterContSat - VolFracF)
% hold off
% legend('frozen','liquid')

%% Make a selection of Tfreeze
Tselec = [273.05 272.85 271.85 271 269 267];
d = (-A/(6.0*pi*densW*Lf) .* (Tm ./ (Tm - Tselec))).^(1/3);
% Minimum Pore radii that will freeze at the given T
% All radii > Rfreeze will be frozen
Rfreeze = sigmaLs ./ densW ./ Lf .* (Tm ./ (Tm - Tselec)) + d;

VolFracFreezing = zeros(length(Rfreeze),1);
for i = 1:length(Rfreeze)
  for j=1:length(R)
    if R(j) > Rfreeze(i)
      VolFracFreezing(i) = VolFracFreezing(i) + pi * N(j) * (R(j)-d(i))^2;
    end
  end
end

Tselec(1) = 273.15;
%%
figure()
plot(Tselec, VolFracFreezing)
hold on
plot(Tselec,maxWaterContSat - VolFracFreezing)
hold off
legend('frozen','liquid')
%%
% save('freezing_poreRadii.mat','Tselec','VolFracFreezing');
