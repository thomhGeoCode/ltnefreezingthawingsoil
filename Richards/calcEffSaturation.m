% File written by Thomas Heinze, Bochum, Germany
% Copyright (c) 2021 Thomas Heinze
% Please cite as: 
% Thomas Heinze (2021) A multi-phase heat transfer model for water infiltration into frozen soil

function EffSat = calcEffSaturation(WaterCont, waterContRes, WaterContSat)

  EffSat = ( WaterCont - waterContRes ) ./ (WaterContSat - waterContRes);

end