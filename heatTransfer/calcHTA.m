% File written by Thomas Heinze, Bochum, Germany
% Copyright (c) 2021 Thomas Heinze
% Please cite as: 
% Thomas Heinze (2021) A multi-phase heat transfer model for water infiltration into frozen soil

function hta = calcHTA(Radius, VolumeFraction)

  hta = 2.0 .* VolumeFraction ./ Radius;
  
end