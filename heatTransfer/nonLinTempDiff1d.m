% File written by Thomas Heinze, Bochum, Germany
% Copyright (c) 2021 Thomas Heinze
% Please cite as: 
% Thomas Heinze (2021) A multi-phase heat transfer model for water infiltration into frozen soil

function NewValue = nonLinTempDiff1d(Value, VolFrac, density, heatCapac,...
  thermalConduc, source1, source2, dt, dz)

  alpha = dt ./ ( VolFrac .* density .* heatCapac);

  aa = ( VolFrac(2:end-1) .* thermalConduc + ...
       VolFrac(3:end) .* thermalConduc ) .* 0.5;
     
  bb = ( VolFrac(2:end-1) .* thermalConduc + ...
         VolFrac(1:end-2) .* thermalConduc ) .* 0.5;
  
  Ta = Value(3:end) - Value(2:end-1);
  
  Tb = Value(2:end-1) - Value(1:end-2);
  
  NewValue = Value;
  
  NewValue(2:end-1) = Value(2:end-1) + alpha(2:end-1) .* ( aa .* Ta - bb .* Tb)...
    ./ dz ./ dz + alpha(2:end-1) .* ( source1(2:end-1) + source2(2:end-1));
  
end