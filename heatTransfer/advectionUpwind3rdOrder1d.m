% File written by Thomas Heinze, Bochum, Germany
% Copyright (c) 2021 Thomas Heinze
% Please cite as: 
% Thomas Heinze (2021) A multi-phase heat transfer model for water infiltration into frozen soil

function NewValue = advectionUpwind3rdOrder1d(Value, Velocity, dt, dz)

  NewValue = Value;

  NewValue(3:end-1) = Value(3:end-1) - dt .* Velocity(3:end-1) .* ...
    (2.0 .* Value(4:end) + 3.0.*Value(3:end-1) - 6.0 .* Value(2:end-2) ...
     + Value(1:end-3) ) ./ (6.0 .*  dz);

end