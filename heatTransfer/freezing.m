% File written by Thomas Heinze, Bochum, Germany
% Copyright (c) 2021 Thomas Heinze
% Please cite as: 
% Thomas Heinze (2021) A multi-phase heat transfer model for water infiltration into frozen soil

function [Tl, Ti, VolFracI, VolFracL, ChangeL, FreezingStage] = freezing(VolFracI, VolFracL, ...
  Tliquid, Tice, latentHeat, FreezingStage, Tselec, VolFracFreezing, DensW, heatCapacL, dt)

  % Set phase change temperature and corresponding max freezing volume
  TphaseChange = Tselec(FreezingStage)';
  VolFracIceMax = VolFracFreezing(FreezingStage);
      
  % Calculate available energy due to underestimated temperature
  DeltaT = TphaseChange - Tliquid;
  DeltaT(DeltaT<0.1) = 0.0;
  DeltaT(VolFracI>=VolFracIceMax) = 0.0;
  
  % Calculate available energy
  Q = VolFracL .* DensW .* heatCapacL .* DeltaT;
  
  % Calculate change in liquid water volume fraction due to freezing
  ChangeVolFrac = Q ./ DensW ./ latentHeat;  
  
  % Maximum possible change in ice content based on current Tph
  MaxChange = min(VolFracIceMax - VolFracI, VolFracL);
  
  FreezingStage(ChangeVolFrac > MaxChange) = min(FreezingStage(ChangeVolFrac > MaxChange) + 1,6);
  ChangeVolFrac(ChangeVolFrac > MaxChange) = MaxChange(ChangeVolFrac > MaxChange);
  
  clear MaxChange;
  
  % Ice temperature increased due warmer newly frozen water
  Ti = max(0.0, (VolFracI .* Tice + ChangeVolFrac .* TphaseChange) ./ ...
    ( VolFracI + ChangeVolFrac ));
  
  % Actually used energy for phase change.
  Qtrue = ChangeVolFrac .* DensW .* latentHeat;
  % Set liquid water temperature
  Tl = Tliquid + Qtrue./ ( VolFracL .* DensW .* heatCapacL);
  
  % Updated Ice water content
  VolFracI = VolFracI + ChangeVolFrac;
    
  % Updated liquid water content
  VolFracL = VolFracL - ChangeVolFrac;
  
  % Changing rate (1/s)
  ChangeL = - ChangeVolFrac./dt;

end