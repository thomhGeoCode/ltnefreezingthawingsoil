% File written by Thomas Heinze, Bochum, Germany
% Copyright (c) 2021 Thomas Heinze
% Please cite as: 
% Thomas Heinze (2021) A multi-phase heat transfer model for water infiltration into frozen soil

function htc = calcHTC(ThermConduc, Radius, Re,Pr)

  htc = ThermConduc./Radius .* (2.4e-5 + 285.6 .* Pr.^(0.3) .* Re.^(2.7));

end
