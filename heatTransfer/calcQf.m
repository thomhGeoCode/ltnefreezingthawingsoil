% File written by Thomas Heinze, Bochum, Germany
% Copyright (c) 2021 Thomas Heinze
% Please cite as: 
% Thomas Heinze (2021) A multi-phase heat transfer model for water infiltration into frozen soil

function Q = calcQf(Tr, Tg, VolFracG, VolFracL, VolFracF, ...
  PrGas, ViscGas, ThermConducGas, DensityGas, VelocityGas, IceCoreRadius)

  Re = calcReynoldsNr(ViscGas, IceCoreRadius, DensityGas, VelocityGas);
  h = calcHTC(ThermConducGas, IceCoreRadius, Re,PrGas);
  Af = calcHTA(IceCoreRadius, VolFracF);
  A = VolFracG ./ ( VolFracG + VolFracL) .* Af;
  
  Q = h .* A .* (Tg - Tr);
  
  Q(isnan(Q)) = 0.0;
  Q(VolFracG < 0.005) = 0.0;
  Q(VolFracF < 0.005) = 0.0;
  
end