% File written by Thomas Heinze, Bochum, Germany
% Copyright (c) 2021 Thomas Heinze
% Please cite as: 
% Thomas Heinze (2021) A multi-phase heat transfer model for water infiltration into frozen soil

function [Tr, Tl, Tg, Tf, VolFracF, VolFracL] = thermalCalculations(...
  surfAreaRock, PoreRadius, PrL, PrG, VolFracG, VolFracL, VolFracF, Trock, Tliquid, Tgas, Tice)

  % Calculate heat transfer terms
  Qrg = calcQr(Trock, Tgas, VolFracG, surfAreaRock, PoreRadius, ...
    VolFracL, PrG, ViscG, DensG, Vel);

  Qrl = calcQr(Trock, Tliquid, VolFracL, surfAreaRock, PoreRadius, ...
    VolFracG, PrL, ViscL, DensW, Vel);
  
  Qfl = calcQf(Tice, Tliquid, VolFracL, VolFracG, VolFracF, PrL, ViscL,...
    DensW, VelL, IceCoreRadius);
  
  Qfg = calcQf(Tice, Tgas, VolFracG, VolFracL, VolFracF, PrG, ViscG, ...
    DensG, VelG, IceCoreRadius);
  
  % Update phase temperatures
  % soil
  Tr = nonLinTempDiff1d(Trock, VolFracR, DensR, heatCapacR,...
    thermalConducR, Qrg, Qrl, 0.0, dt, dz);

  Tr(1) = 0.0;
  Tr(end) = Tr(end-1);
  
  % air
  Tg = nonLinTempDiff1d(Tgas, VolFracG, DensG, heatCapacG,...
    thermalConducG, -Qrg, -Qfg, dt, dz);
  
  Tg(1) = 0.0;
  Tg(end) = Tg(end-1);
  
  Tg = advectionUpwind3rdOrder1d(Tg, VelG, dt, dz);
  
  Tg(1) = 0.0;
  Tg(2) = 0.0;
  Tg(end) = Tg(end-1);

  % Liquid water
  Tl = nonLinTempDiff1d(Tliquid, VolFracL, DensW, heatCapacL,...
    thermalConducL, -Qrl, -Qfl, dt, dz);
  
  Tl(1) = 2.0;
  Tl(end) = Tl(end-1);
  
  Tl = advectionUpwind3rdOrder1d(Tl, VelL, dt, dz);
  
  Tl(1) = 0.0;
  Tl(2) = 0.0;
  Tl(end) = Tl(end-1);
  
  % Frozen water
  Tf = nonLinTempDiff1d(Tice, VolFracF, DensW, heatCapacF,...
    thermalConducF, Qfl, Qfg, dt, dz);
  
  Tf(1) = -3.0;
  Tf(end) = Tf(end-1);

  % Phase Change
  if (min(Tl) < TphaseChange)
    [Tl, Tf, VolFracF, VolFracL] = freezing(VolFracF, VolFracL, ...
      Tl, Tf, latentHeat, TphaseChange, DensW, heatCapacL, dt);
  end

  if (max(Tf) > TphaseChange)
    [Tf, Tl, VolFracF, VolFracL] = melting(TiceOld, VolFracF, ...
      VolFracL, latentHeat, TphaseChange, DensW, heatCapacF, dt);
  end

end