% File written by Thomas Heinze, Bochum, Germany
% Copyright (c) 2021 Thomas Heinze
% Please cite as: 
% Thomas Heinze (2021) A multi-phase heat transfer model for water infiltration into frozen soil

function [Ti, Tl, VolFracF, VolFracL, ChangeL, FreezingStage] = melting(Tice, Tliquid, VolFracF, ...
  VolFracL, latentHeat, FreezingStage, Tselec, VolFracFreezing, DensW, heatCapacF, dt)

  TphaseChange = Tselec(FreezingStage)';
  VolFracIceMax = VolFracFreezing(FreezingStage);
  
  % Calculate available energy due to overestimated temperature
  DeltaT = Tice - TphaseChange;
  DeltaT(DeltaT<0.1) = 0.0;

  % Calculate available energy
  Q = VolFracF .* DensW .* heatCapacF .* DeltaT;
  
  % Calculate change in liquid water volume fraction due to freezing
  ChangeVolFrac = Q ./ DensW ./ latentHeat;  
  
  % Maximum possible change in ice content
  ChangeVolFrac(ChangeVolFrac>VolFracF) = VolFracF(ChangeVolFrac>VolFracF);
  
  % Ice temperature increased due colder melted ice
  Tl = (VolFracL .* Tliquid + ChangeVolFrac .* TphaseChange) ./ ...
    ( VolFracL + ChangeVolFrac );
  
  % Actually used energy for phase change.
  Qtrue = ChangeVolFrac .* DensW .* latentHeat;
  % Set liquid water temperature
  Ti = Tice;
  Ti(VolFracF>0) = Tice(VolFracF>0) - Qtrue(VolFracF>0)./ ( VolFracF(VolFracF>0) .* DensW .* heatCapacF);
  
  % Updated Ice water content
  VolFracF = VolFracF - ChangeVolFrac;
%   Ti(VolFracF<0.01) = 0.0;
%   VolFracF(VolFracF < 0.01) = 0.0;
    
  % Updated liquid water content
  VolFracL = VolFracL + ChangeVolFrac;
  
  % Changing rate (1/s)
  ChangeL = ChangeVolFrac./dt;
  
  % Update Freezing Stage
  FreezingStage(VolFracF < VolFracIceMax) = max(1, FreezingStage(VolFracF < VolFracIceMax) - 1);

end