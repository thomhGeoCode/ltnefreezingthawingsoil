% File written by Thomas Heinze, Bochum, Germany
% Copyright (c) 2021 Thomas Heinze
% Please cite as: 
% Thomas Heinze (2021) A multi-phase heat transfer model for water infiltration into frozen soil

function NewValue = nonLinTempDiff1dPartial(Value, VolFrac, density, heatCapac,...
  thermalConduc, source1, source2, dt, dz)

  global limit

alpha = 0.0 * VolFrac;
  alpha(VolFrac>limit) = dt ./ ( VolFrac(VolFrac>limit) .* density .* heatCapac);

  NewValue = Value;
  
  for i=2:length(Value)-1

    if VolFrac(i) > limit
      
      if VolFrac(i-1) > limit && VolFrac(i+1) > limit
        NewValue(i) = Value(i) + alpha(i) * ( ...
          ( VolFrac(i+1) + VolFrac(i) ) * 0.5 * thermalConduc * ...
          ( Value(i+1) - Value(i) ) - ...
          ( VolFrac(i-1) + VolFrac(i) ) * 0.5 * thermalConduc * ...
          ( Value(i) - Value(i-1) ) ) /dz /dz + ...
          alpha(i) * ( source1(i) + source2(i) );
      elseif VolFrac(i-1) > limit && VolFrac(i+1) <= limit
        NewValue(i) = Value(i-1) + alpha(i) * ( source1(i) + source2(i) );
      elseif VolFrac(i-1) <= limit && VolFrac(i+1) > limit
        NewValue(i) = Value(i+1) + alpha(i) * ( source1(i) + source2(i) );
      else
        NewValue(i) = Value(i) + alpha(i) * ( source1(i) + source2(i) );
      end
      
    else
      NewValue(i) = 0.0;
    end
    
  end

end