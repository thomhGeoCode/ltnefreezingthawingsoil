% File written by Thomas Heinze, Bochum, Germany
% Copyright (c) 2021 Thomas Heinze
% Please cite as: 
% Thomas Heinze (2021) A multi-phase heat transfer model for water infiltration into frozen soil

function Qrg = calcQr(Tr, Tg, VolFracG, SurfArea, PoreRadius, ...
  VolFracL, PrGas, ViscGas, ThermConducGas, DensityGas, VelocityGas)

  global limit

  Re = calcReynoldsNr(ViscGas, PoreRadius, DensityGas, VelocityGas);
  hrg = calcHTC(ThermConducGas, PoreRadius, Re,PrGas);
  Arg = SurfArea .* VolFracG ./ (VolFracG + VolFracL);
  Qrg = hrg .* Arg .* (Tg - Tr);
  
  Qrg(isnan(Qrg)) = 0.0;
  Qrg(VolFracG < limit) = 0.0;
end