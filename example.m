% Exemplary main script for freezing/thawing of soil
% written by Thomas Heinze, Bochum, Germany
% Copyright (c) 2021 Thomas Heinze
% Please cite as: 
% Thomas Heinze (2021) A multi-phase heat transfer model for water infiltration into frozen soil

clear all
close all
clc

%% Add path
addpath('./heatTransfer')
addpath('./Richards')

%% User specified input
Qext = 1.5e3; 

% Simulation specific
% length of column (m)
height = 0.35; 
% number of grid points (-)
nz = 36;
% total length of simulation (s)
maxTime = 12.0*60.0*60.0+1.0;
% simulation time step (s)
dt = 0.01;

% dynamic viscosity of water 
viscL = 1.7e-3;
% dynamic viscosity of air (Pa s)
viscG = 1.7e-5;

% density of water (kg/m^3)
densW = 1000.0;
% density of air (kg/m^3)
densG = 1.2;
% density of rock (kg/m^3)
densR = 1180.0;
% heat capacity of liquid water (J/kg/K)
heatCapacL = 4200.0;
% heat capacity of air (J/kg/K)
heatCapacG = 1008.0;
% heat capacity of ice (J/kg/K)
heatCapacF = 2040.0;
% heat capacity of rock (J/kg/K)
heatCapacR = 887.0;
% thermal conductitity of liquid water (W/m/K)
thermConducL= 0.55;
% thermal conductivity of air (W/m/K)
thermConducG = 0.024;
% thermal conductivity of ice (W/m/K)
thermConducF = 2.2;
% thermal conductivity of rock (W/m/K)
thermConducR = 0.97;

% Mean pore radius of soil (m)
poreRadius = 1.2e-5;
% Porosity of soil (-)
porosity = 0.437;
% Hydraulic conductivity [m/s]
hydConducSat = 2.0/(24.0*60.0*60.0);
% residual water content [-]
waterContRes = 0.06;
% maximum water content in the soil
maxWaterContSat = porosity;
% vanGenuchten parameter
vanGenAlpha = 1.3;
vanGenN = 1.25;
vanGenM = 1.0 - (1.0/vanGenN);

% latent heat of fusion (J/kg)
latentHeat = 334.0e3;

% grid spacing (m)
dz = height / (nz-1);
% time (s)
time = 0.0;
% specific surface area of rock (1/m)
surfAreaRock = calcHTA(poreRadius, (1.0 - porosity));
% Prandtl number liquid water (-)
prL = calcPrandtlNr(heatCapacL, viscL, thermConducL);
% Prandtl number air (-)
prG = calcPrandtlNr(heatCapacG, viscG, thermConducG);

% time counting
seconds = 0.0;
minutes = 0;
hours = 0;
time_count = 0;

global  limit
limit = 0.0005;

%% Initialize arrays
% incorporate international Wc from lab data
VolFracL = 0.4 .* ones(nz,1);
% Saturated water content
WaterContSat = maxWaterContSat * ones(nz,1);
% effective saturation [-]
EffSat = calcEffSaturation(VolFracL, waterContRes, WaterContSat);
% pressure head [m]
PressureHead = - (EffSat.^(-1.0/vanGenM) - 1.0).^(1.0/vanGenN) ./ vanGenAlpha;
% water content [-]
VolFracL = calcWaterContent(PressureHead, waterContRes, ...
  WaterContSat, vanGenAlpha, vanGenN, vanGenM);
% effective hydraulic conductivity [m/s]
EffHydConduc = calcEffHydConduc(EffSat, hydConducSat, vanGenM, 0.0);
% moisture capacity (1/m)
MoistCapac = calcMoistCapac(EffSat, waterContRes, WaterContSat,...
  vanGenAlpha, vanGenM);
% flow velocity (m/s)
Vel = calcDarcyVel(EffHydConduc, PressureHead, dz);
% source/sink term in Richards equ. (1/s)
ChangeL = 0.0 .* PressureHead;
ChangeLm = 0.0.*ChangeL;
ChangeLf = 0.0.*ChangeL;

% temperature of liquid water (K)
TempL = 4.0 .* ones(nz,1) + 273.15;
% temperature of air (K)
TempG = TempL;
% temperature of ice (K)
TempF = 0.0 * ones(nz,1);
% temperature of rock (K)
TempR = TempL;
% volume fraction of air (-)
VolFracG = porosity - VolFracL;
% volume fraction of ice (-)
VolFracF = 0.0 * ones(nz,1);
% volume fraction solid (-)
VolFracR = (1.0 - porosity) * ones(nz,1); 
% Radius of ice core (m)
IceCoreRadius = calcIceCoreRadius(poreRadius, VolFracF, porosity);
% Freezing stage to determine phase change temp & freezing volume
FreezingStage = ones(nz,1);
% heat transfer rock - fluid
Qrl = calcQr(TempR, TempL, VolFracL, surfAreaRock, poreRadius, ...
	VolFracG, prL, viscL, thermConducG, densW, Vel);
% heat transfer rock - air
Qrg = calcQr(TempR, TempG, VolFracG, surfAreaRock, poreRadius, ...
	VolFracL, prG, viscG, thermConducG, densG, Vel);
% heat transfer ice - fluid
Qfl = calcQf(TempF, TempF, VolFracL, VolFracG, VolFracF, prL, viscL,...
	thermConducL, densW, Vel, IceCoreRadius);
% heat transfer ice - air
Qfg = calcQf(TempF, TempG, VolFracG, VolFracL, VolFracF, prG, viscG, ...
  thermConducG, densG, Vel, IceCoreRadius);

% Import pore radii and respective freezing temperatures
[Tselec, VolFracFreezing] = freezingRadii(waterContRes, ...
  maxWaterContSat, vanGenAlpha, vanGenN, vanGenM);

% saving
saveTr = zeros(36,12);
saveTl = saveTr;
saveTf = saveTr;
saveTg = saveTr;
saveVFl = saveTr;
saveVFg = saveTr;
saveVFf = saveTr;
svidx = 1;

pressureTop = PressureHead(1);
pressureBottom = PressureHead(end);

%% start the simulation
while time < maxTime

  time_count = time_count + 1;
  
  % flow model
  EffHydConduc = calcEffHydConduc(EffSat, hydConducSat, vanGenM, VolFracF);
  
  MoistCapac = calcMoistCapac(EffSat, waterContRes, WaterContSat,...
    vanGenAlpha, vanGenM);

  PressureHead = calcPressureHead1d(PressureHead, EffHydConduc, ...
    MoistCapac, ChangeL, dt, dz);
  
  PressureHead(1) = pressureTop;
  PressureHead(end) = pressureBottom;
  
  Vel = calcDarcyVel(EffHydConduc, PressureHead, dz);
  
  VolFracL = calcWaterContent(PressureHead, waterContRes, ...
    WaterContSat, vanGenAlpha, vanGenN, vanGenM);
  
  EffSat = calcEffSaturation(VolFracL, waterContRes, WaterContSat);
  
  VolFracG = porosity - VolFracL - VolFracF;
  
  %% heat model
  % Calculate heat transfer terms
  IceCoreRadius = calcIceCoreRadius(poreRadius, VolFracF, porosity);
  
  % Update phase temperatures
  alphaR = dt ./ ( VolFracR .* densR .* heatCapacR);
  alphaL = 0.0 .* alphaR;
  alphaG = 0.0 .* alphaR;
  alphaF = 0.0 .* alphaR;
  alphaL(VolFracL>limit) = dt ./ ( VolFracL(VolFracL>limit) .* densW .* heatCapacL);
  alphaG(VolFracG>limit) = dt ./ ( VolFracG(VolFracG>limit) .* densG .* heatCapacG);
  alphaF(VolFracF>limit) = dt ./ ( VolFracF(VolFracF>limit) .* densW .* heatCapacF);  
  
  % soil
  TempR = nonLinTempDiff1d(TempR, VolFracR, densR, heatCapacR,...
    thermConducR, zeros(nz,1), zeros(nz,1), dt, dz);
  
  TempR(1) = TempR(1) + alphaR(1) * Qext * VolFracR(1) * (273.15 - 7 - TempR(1) );
  TempR(end) = TempR(end) + alphaR(end) * 0.1 * Qext * VolFracR(end) * (273.15 + 2 - TempR(end) );

  TempG = nonLinTempDiff1dPartial(TempG, VolFracG, densG, heatCapacG,...
    thermConducG, zeros(nz,1), zeros(nz,1), dt, dz);
  
  if VolFracG(1) > 0.0
    TempG(1) = TempG(1) + alphaG(1) * Qext * VolFracG(1) * (273.15 - 7 - TempG(1) );
  end
  if VolFracG(end) > 0.0
    TempG(end) = TempG(end) + alphaG(end) * 0.1 * Qext * VolFracG(end) * (273.15 + 2 - TempG(end) );
  end
  
  TempG = advectionUpwind3rdOrder1d(TempG, Vel, dt, dz);
  
  TempL = nonLinTempDiff1dPartial(TempL, VolFracL, densW, heatCapacL,...
    thermConducL, zeros(nz,1), zeros(nz,1), dt, dz);
  
  if VolFracL(1) > 0.0
    TempL(1) = TempL(1) + alphaL(1) * Qext * VolFracL(1) * (273.15 - 7 - TempL(1) );
  end
  if VolFracL(end) > 0.0
    TempL(end) = TempL(end) + alphaL(end) * 0.1 * Qext * VolFracL(end) * (273.15 + 2 - TempL(end) );
  end
  
  TempL = advectionUpwind3rdOrder1d(TempL, Vel, dt, dz);
  
  TempF = nonLinTempDiff1dPartial(TempF, VolFracF, densW, heatCapacF,...
    thermConducF, zeros(nz,1), zeros(nz,1), dt, dz);
  
  if VolFracF(1) > 0.0
    TempF(1) = TempF(1) + alphaF(1) * Qext * VolFracF(1) * (273.15 - 7 - TempF(1) );
  end
  if VolFracF(end) > 0.0
    TempF(end) = TempF(end) + alphaF(end) * 0.1 * Qext * VolFracF(end) * (273.15 + 2 - TempF(end) );
  end
  
  % Heat transfer between phases
  Qrg = calcQr(TempR, TempG, VolFracG, surfAreaRock, poreRadius, ...
    VolFracL, prG, viscG, thermConducG, densG, Vel);
  
  TempR = TempR + alphaR .* Qrg;
  TempG = TempG - alphaG .* Qrg;
  
  Qrl = calcQr(TempR, TempL, VolFracL, surfAreaRock, poreRadius, ...
    VolFracG, prL, viscL, thermConducG, densW, Vel);
  
  TempR = TempR + alphaR .* Qrl;
  TempL = TempL - alphaL .* Qrl;
  
  % Qfl by definition has to be larger than 0 because TempL > TempF for all
  % times. BUT: Due to the limit for freezing, it might happen that TempL<
  % TempF before it can freeze again.
  Qfl = calcQf(TempF, TempL, VolFracL, VolFracG, VolFracF, prL, viscL,...
    thermConducL, densW, Vel, IceCoreRadius);
  Qfl(Qfl<=0) = 0.0;
  
  TempF = TempF + alphaF .* Qfl;
  TempL = TempL - alphaL .* Qfl;
  
  Qfg = calcQf(TempF, TempG, VolFracG, VolFracL, VolFracF, prG, viscG, ...
    thermConducG, densG, Vel, IceCoreRadius);
  
  TempF = TempF + alphaF .* Qfg;
  TempG = TempG - alphaG .* Qfg;

  % Phase Change
  ChangeLm = 0.0 .* ChangeLm;
  ChangeLf = 0.0 .* ChangeLf;
  if (min(nonzeros(TempL)) < 273.15)
    [TempL, TempF, VolFracF, VolFracL, ChangeLf, FreezingStage] = freezing(VolFracF, VolFracL, ...
      TempL, TempF, latentHeat, FreezingStage, Tselec, VolFracFreezing, densW, heatCapacL, dt);
    
    WaterContSat = maxWaterContSat - VolFracF;
    
    EffSat = calcEffSaturation(VolFracL, waterContRes, WaterContSat);
  end

  if (max(TempF) > 273.15)
    [TempF, TempL, VolFracF, VolFracL, ChangeLm, FreezingStage] = melting(TempF, TempL, VolFracF, ...
      VolFracL, latentHeat, FreezingStage, Tselec, VolFracFreezing, densW, heatCapacF, dt);

    WaterContSat = maxWaterContSat - VolFracF;

    EffSat = calcEffSaturation(VolFracL, waterContRes, WaterContSat);
  end
  ChangeL = ChangeLm + ChangeLf;

  %% housekeeping
  time = time + dt;
  
  seconds = seconds + dt;
  
  if (seconds >= 60.0)
    minutes = minutes + 1;
    seconds = seconds - 60.0;
    if (minutes >=60)
      hours = hours + 1;
      minutes = 0;
    end
  end
  
  %% saving
  if ( seconds < dt && minutes == 0 )
    saveTr(:,svidx) = TempR;
    saveTl(:,svidx) = TempL;
    saveTg(:,svidx) = TempG;
    saveTf(:,svidx) = TempF;
    saveVFl(:,svidx) = VolFracL;
    saveVFg(:,svidx) = VolFracG;
    saveVFf(:,svidx) = VolFracF;
    
    svidx = svidx + 1;
  end
  
end
