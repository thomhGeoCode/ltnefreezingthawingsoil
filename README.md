# LTNEfreezingthawingSoil

This repository contains all functions and an exemplary execution script for the local thermal non-equilibrium model for water infiltration into freezing soil presented in:

Heinze, T. (2021). A multi-phase heat transfer model for water infiltration into frozen soil. Water Resources Research, 57, e2021WR030067. https://doi.org/10.1029/2021WR030067
